<h1>1. Requirements</h1>
<ul>
    <li>PHP >= 5.5.9</i>
    <li>OpenSSL PHP Extension</li>
    <li>PDO PHP Extension</li>
    <li>Mbstring PHP Extension</li>
    <li>Tokenizer PHP Extension</li>
    <li>Fileinfo PHP Extension</li>
    <li>GD PHP Extension</li>
</ul>

<h1>2. Make sure the following files & folders are writable : </h1>
<ul>
    <li> Storage/app/ </li>
    <li> Storage/framework/ </li>
    <li> Storage/logs/ </li>
    <li> Storage/cache/ </li>
    <li> bootstrap/cache/ </li>
    <li> .env </li>
</ul>
    
<h1>3. Create database with name attendize, then execute dump sql 'attendize-sdb2018-4-6-18.sql'</h1>

<h1>4. Attendize uses Wkhtml2PDF to generate tickets. If you are getting errors while generating PDFs make sure all the driver files in vendor\nitmedia\wkhtml2pdf\src\Nitmedia\Wkhtml2pdf\lib executable.
Also make sure the setting for WKHTML2PDF_BIN_FILE is correct in the .env file. The acceptable options are:</h1>
<ul>    
    <li>wkhtmltopdf-0.12.1-OS-X.i386 - Mac OS X 10.8+ (Carbon), 32-bit</li>
    <li>wkhtmltopdf-amd64 - Linux (Debian Wheezy), 64-bit, for recent distributions (i.e. glibc 2.13 or later)</li>
    <li>wkhtmltopdf-i386 - Linux (Debian Wheezy), 32-bit, for recent distributions (i.e. glibc 2.13 or later)</li>
</ul>
<h1>5. hoorray, now you can run it </h1>
><i>php artisan serve</i>

