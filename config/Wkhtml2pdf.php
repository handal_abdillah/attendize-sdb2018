<?php

return [

    'debug'       => env('APP_DEBUG_PDF', false),
    'binpath'     => 'lib/',
    'binfile'     => env('WKHTML2PDF_BIN_FILE', 'wkhtmltopdf-0.12.1-OS-X.i386'),
    'output_mode' => 'I',
];

// return [

//     'debug'       => env('APP_DEBUG_PDF', true),
//     'binpath' => 'lib/',
//     'binfile' => 'wkhtmltopdf-amd64',
//     'output_mode' => 'I'
// ];
